#include "Color.h"

Color::Color(uint8_t r=1, uint8_t g=1, uint8_t b=1, uint8_t a=1)
{
    R = r;
    G = g;
    B = b;
    A = a;
}

uint32_t Color::toUint32() const
{
    return (A << 24) | (R << 16) | (G << 8) | B;
}

Color Color::operator+(const Color &c) const
{
    // Check overflows
    short int a = (A + c.A) % UINT8_MAX,
              r = (R + c.R) % UINT8_MAX,
              g = (G + c.G) % UINT8_MAX,
              b = (B + c.B) % UINT8_MAX;

    return Color(r, g, b, a);
}

Color Color::operator-(const Color &c) const
{
    // Allow negative values for calculation
    short int a = A - c.A,
              r = R - c.R,
              g = G - c.G,
              b = B - c.B;

    if (a < 0) a = 0;
    if (r < 0) r = 0;
    if (g < 0) g = 0;
    if (b < 0) b = 0;

    return Color(r, g, b, a);
}

Color Color::operator*(const uint8_t m) const
{
    // Check overflows
    short int a = (A * m) % UINT8_MAX,
              r = (R * m) % UINT8_MAX,
              g = (G * m) % UINT8_MAX,
              b = (B * m) % UINT8_MAX;

    return Color(r, g, b, a);
}
