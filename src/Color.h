#ifndef __COLOR_H__
#define __COLOR_H__

#include <stdint.h>

class Color
{
public:
    /* uint8_t is used purely to pack without loss or overflow onto uint32_t */
    uint8_t R, G, B, A;

    Color(uint8_t r, uint8_t g, uint8_t b, uint8_t a);

    /**
     * Convert separate color values into single uint32_t packed value.
     *
     * @return {uint32_t} 0..7 A, 8..15 R, 16..23 G, 24..31 B encoded value
     */
    uint32_t toUint32() const;
    Color operator + (const Color &c) const;
    Color operator - (const Color &c) const;
    Color operator * (const uint8_t m) const;
};

#endif /*__COLOR_H__*/
