#include "gtest/gtest.h"
#include "Color.cpp"

/* Test default set values for constructor */
TEST(ColorCtor, ctor_default)
{
    const Color color;

    EXPECT_EQ(1, color.A);
    EXPECT_EQ(1, color.R);
    EXPECT_EQ(1, color.G);
    EXPECT_EQ(1, color.B);
}

/* Set custom values for constructor */
TEST(ColorCtor, ctor_manual)
{
    const Color color(2, 3, 4, 5);

    EXPECT_EQ(5, color.A);
    EXPECT_EQ(2, color.R);
    EXPECT_EQ(3, color.G);
    EXPECT_EQ(4, color.B);
}

/* Add values in normal case scenario */
TEST(ColorAdd, add_normal)
{
    const Color color1(1,2,3,4),
                color2(5,6,7,8);

    const Color color_res = color1 + color2;

    EXPECT_EQ(12, color_res.A);
    EXPECT_EQ(6, color_res.R);
    EXPECT_EQ(8, color_res.G);
    EXPECT_EQ(10, color_res.B);
}

/* Overflow single value */
TEST(ColorAdd, add_overflow_one)
{
    const Color color1(UINT8_MAX, 0, 0, 0),
                color2(200, 100, 100, 100);

    const Color color_res = color1 + color2;

    EXPECT_EQ(100, color_res.A);
    EXPECT_EQ(200, color_res.R);
    EXPECT_EQ(100, color_res.G);
    EXPECT_EQ(100, color_res.B);
}

/* Overflow all values */
TEST(ColorAdd, add_overflow_all)
{
    const Color color1(UINT8_MAX, UINT8_MAX, UINT8_MAX, UINT8_MAX),
                color2(10, 20, 30, 40);
    const Color color_res = color1 + color2;

    EXPECT_EQ(40, color_res.A);
    EXPECT_EQ(10, color_res.R);
    EXPECT_EQ(20, color_res.G);
    EXPECT_EQ(30, color_res.B);
}

/* Subtract values in normal use case */
TEST(ColorSub, sub_normal)
{
    const Color color1(10, 20, 30, 40),
                color2(1, 2, 3, 4);
    const Color color_res = color1 - color2;

    EXPECT_EQ(36, color_res.A);
    EXPECT_EQ(9, color_res.R);
    EXPECT_EQ(18, color_res.G);
    EXPECT_EQ(27, color_res.B);
}

/* Subtract values where one result is negative - expect 0 */
TEST(ColorSub, sub_negative_one)
{
    const Color color1(10, 20, 30, 40),
                color2(11, 2, 3, 4);
    const Color color_res = color1 - color2;

    EXPECT_EQ(36, color_res.A);
    EXPECT_EQ(0, color_res.R);
    EXPECT_EQ(18, color_res.G);
    EXPECT_EQ(27, color_res.B);
}

/* Subtract values where all results are negative - expect 0 */
TEST(ColorSub, sub_negative_all)
{
    const Color color1(1, 2, 3, 4),
                color2(2, 3, 4, 5);
    const Color color_res = color1 - color2;

    EXPECT_EQ(0, color_res.A);
    EXPECT_EQ(0, color_res.R);
    EXPECT_EQ(0, color_res.G);
    EXPECT_EQ(0, color_res.B);
}
